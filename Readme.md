

# Mouse Gamepad



Enable to move the mouse cursor  with the gamepad.



## Prerequisites



Don't forget to set the gamepad to the control

http://www.stencyl.com/help/view/gamepads/





## Stencyl Blocks



![enable_gamepad](doc/enable_gamepad.png)

Enable the mouse gamepad for the scene or actor.



![set_to_control](doc/set_gamepad_mouse.png)

Set the speed of the Gamepad mouse. Default is 1.



![set_to_control](doc/set_to_control.png)

Joystick down: the mouse moves down 