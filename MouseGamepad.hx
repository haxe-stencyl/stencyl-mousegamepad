package MouseGamepad;

import openfl.events.MouseEvent;
import com.stencyl.behavior.Script.*;
import com.stencyl.behavior.Script;
import com.stencyl.Input;
import com.stencyl.Engine;


@:access(Universal)
class MouseGamepad
{
	public static var joystickDown  : String;
	public static var joystickUp    : String;
	public static var joystickLeft  : String;
	public static var joystickRight : String;

	public static var clickControl  : String;

	public static var maxSpeed      : Float = 1;

	public static function enableGamepadMouse(s:Script)
	{
		// Moving mouse
		s.addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void
		{
			var hasJoyStickMoved = false;
			var x:Int = Std.int(Engine.stage.mouseX);
			var y:Int = Std.int(Engine.stage.mouseY);

			if (isKeyDown(joystickDown))
			{
				y += Std.int(elapsedTime * Input.getButtonPressure(joystickDown) * maxSpeed);
				hasJoyStickMoved = true;
			}
			
			if (isKeyDown(joystickUp))
			{
				y -= Std.int(elapsedTime * Input.getButtonPressure(joystickUp) * maxSpeed);
				hasJoyStickMoved = true;
			}

			if (isKeyDown(joystickRight))
			{
				x += Std.int(elapsedTime * Input.getButtonPressure(joystickRight) * maxSpeed);
				hasJoyStickMoved = true;
			}

			if (isKeyDown(joystickLeft))
			{
				x -= Std.int(elapsedTime * Input.getButtonPressure(joystickLeft) * maxSpeed);
				hasJoyStickMoved = true;
			}
			

			// mouse musn't go out of the screen otherwise for lime.ui.Mouse.warp to work well

			if (x < 0)
			{
				x = 0;
			}
			else if (x >= getScreenWidth()-1)
			{
				x = Std.int(getScreenWidth()-1);
			}

			if (y <0)
			{
				y = 0;
			}
			else if (y >= getScreenHeight()-1)
			{
				y = Std.int(getScreenHeight()-1);
			}

			if (hasJoyStickMoved &&( (x != Engine.stage.mouseX) || (y != Engine.stage.mouseY)))
			{
				#if(openfl >= "5.0.0")
				lime.ui.Mouse.warp(Std.int(x),Std.int(y), Universal.window);
				#end
			}
		});

		// Mouse click

		s.addKeyStateListener(clickControl, function(pressed:Bool, released:Bool, list:Array<Dynamic>):Void
		{
			if (pressed)
			{
				var e:MouseEvent = new MouseEvent(MouseEvent.MOUSE_DOWN, true, false);
				Engine.stage.dispatchEvent(e);
				var listeners = Engine.engine.whenMousePressedListeners;
			
				if(listeners != null)
				{
					Engine.invokeListeners(listeners);
				}	
			}
			else if (released)
			{
				var e:MouseEvent = new MouseEvent(MouseEvent.MOUSE_UP, true, false);
				Engine.stage.dispatchEvent(e);
				var listeners = Engine.engine.whenMouseReleasedListeners;
			
				if(listeners != null)
				{
					Engine.invokeListeners(listeners);
				}
			}
		});
	}
}
